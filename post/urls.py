from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.urlpatterns import format_suffix_patterns

from .views import PostView, PostDetail


app_name = "post"

urlpatterns = [
    path("", PostView.as_view()),
    path("<int:pk>/", PostDetail.as_view()),
    path("api-token-auth/", obtain_auth_token, name="api_token_auth"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
