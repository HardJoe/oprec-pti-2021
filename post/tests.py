from django.test import Client, TestCase
from django.urls import resolve, reverse

from .models import Post
from .serializers import PostSerializer
from .views import PostView, PostDetail


class PostsTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.main_url = "/posts/"
        self.test_post = Post.objects.create(
            title="This is title",
            content="This is content"
        )

    def test_post_GET(self):
        response = self.client.get(self.main_url)
        self.assertContains(response, "This is title", status_code=200)

    def test_post_valid_POST(self):
        response = self.client.post(self.main_url, {
            "title": "It's a good day today",
            "content": "Today I got a good score."
        }, follow=True)
        self.assertEquals(response.status_code, 201)

    def test_post_invalid_POST(self):
        response = self.client.post(self.main_url, {
            "title": "Not a valid post",
        }, follow=True)
        self.assertEquals(response.status_code, 400)

    def test_str(self):
        self.assertEqual(str(self.test_post), "This is title")


class DetailTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_post = Post.objects.create(
            title="Is this title",
            content="Is this content"
        )
        self.detail_url = f"/posts/{self.test_post.id}"
        self.invalid_detail_url = "/posts/10000"

    def test_detail_valid_GET(self):
        response = self.client.get(self.detail_url)
        self.assertEquals(response.status_code, 301)

    def test_detail_invalid_GET(self):
        response = self.client.get(self.invalid_detail_url)
        self.assertEquals(response.status_code, 301)

    def test_detail_valid_PUT(self):
        response = self.client.put(self.detail_url, {
            "title": "It's a decent day today",
            "content": "Today I got a decent score."
        }, follow=True)
        self.assertEquals(response.status_code, 200)

    def test_detail_invalid_PUT(self):
        response = self.client.put(self.detail_url, {"title"}, follow=True)
        self.assertEquals(response.status_code, 200)

    def test_detail_DELETE(self):
        response = self.client.delete(self.detail_url, follow=True)
        self.assertEquals(response.status_code, 200)


class SerializerTestCase(TestCase):
    def setUp(self):
        self.test_post = Post.objects.create(
            title="I want to eat",
            content="Rice, muffin, and hamburger"
        )

    def test_update(self):
        new_title = "I want to drink"
        new_content = "Tea, coffee, water"
        dct = {"title": new_title, "content": new_content}
        result = PostSerializer.update(PostSerializer, self.test_post, dct)
        self.assertEquals(result.id, self.test_post.id)
