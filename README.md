# Blog Post API
[![pipeline status](https://gitlab.com/HardJoe/oprec-pti-2021/badges/master/pipeline.svg)](https://gitlab.com/HardJoe/oprec-pti-2021/-/commits/master)
[![coverage report](https://gitlab.com/HardJoe/oprec-pti-2021/badges/master/coverage.svg)](https://gitlab.com/HardJoe/oprec-pti-2021/-/commits/master)

- **This API is available on [http://oprec-pti-jojo.herokuapp.com/](http://oprec-pti-jojo.herokuapp.com/).**
- **Gitlab Repo is on [https://gitlab.com/HardJoe/oprec-pti-2021](https://gitlab.com/HardJoe/oprec-pti-2021).**

# Post
* Post object
```
{
  id: integer
  title: string
  content: string
  date_created: datetime(iso 8601)
}
```
**GET /posts**
----
  Returns all posts in the system.
* **URL Params**  
  None
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Success Response:**  
  * **Code:** 200  
  **Content:**  
```
{
  count: integer,
  posts: [
           {<post_object>},
           {<post_object>},
           {<post_object>}
         ]
}
```

**GET /posts/:id**
----
  Returns the specified post.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Success Response:** 
* **Code:** 200  
  **Content:**  `{ <post_object> }` 
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ "detail": "Not found." }`


**POST /posts**
----
  Creates a new Post and returns the new object.
* **URL Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Data Params**  
```
{
  "title": string,
  "content": string
}
```
* **Success Response:**  
  * **Code:** 201  
    **Content:**  `{ <post_object> }` 
* **Error Response:**  
  * **Code:** 400  
  **Content:** `{ "detail": "JSON parse error - Expecting value: line [integer] column [integer] (char [integer])" }`
  OR
  **Content:** `{ "detail": "JSON parse error - Expecting property name enclosed in double quotes: line [integer] column [integer] (char [integer])" ]`
  OR 
  **Content:**
```
  {
    "<field>": [
        "This field is required."
    ]
}
```
  

**PUT /posts/:id**
----
  Updates fields on the specified post and returns the updated post. All fields must be provided in the request.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
```
{
  "title": string,
  "content": string
}
```
* **Headers**  
  Content-Type: application/json  
* **Success Response:** 
* **Code:** 200  
  **Content:**  `{ <post_object> }` 
* **Error Response:**  
  * **Code:** 400  
  **Content:** `{ "detail": "JSON parse error - Expecting value: line [integer] column [integer] (char [integer])" }`
  OR
  **Content:** `{ "detail": "JSON parse error - Expecting property name enclosed in double quotes: line [integer] column [integer] (char [integer])" ]`
  OR 
  **Content:**
```
  {
    "<field>": [
        "This field is required."
    ]
}
```


**DELETE /posts/:id**
----
  Deletes the specified post.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Success Response:** 
  * **Code:** 204 
  **Content:** `{ "success": "Post with id <id> has been deleted." }` 